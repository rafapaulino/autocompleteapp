package rafapaulino.com.autocompleteapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button botao = (Button) findViewById(R.id.botao);
        botao.setText("Novo texto");


        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicou no botão");
                Toast.makeText(getApplicationContext(),"Funcionou!",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
